package com.mintic.cajero.logica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author Tefa
 */

@Component("estudiante") // Declaración para que spring boot sepa a que modelo hacemos referencia con un Autowired.
public class Estudiante {
    
    @Autowired // Permite la conexión del JdbcTemplate a la base de datos.
    transient JdbcTemplate jdbcTemplate; // Instancia de JdbcTemplate el cual es el equivalente al statement de la clase Conexion
    
    // Atributos (int,float,double,String,boolean)
    private int id;
    private String nombre;
    private String correo;
    private int edad;
    
    
    // Constructor

    public Estudiante(int id, String nombre, String correo, int edad) {
        super();
        this.id = id;
        this.nombre = nombre;
        this.correo = correo;
        this.edad = edad;
    }
    
    public Estudiante(){
        
    }
    
    //Metodos

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
    
    // Método
    public boolean consultar() throws ClassNotFoundException, SQLException{
        //CRUD - R 
        String  sql = "SELECT id,nombre,correo,edad FROM estudiantes WHERE id = ?";
        // Una funcion puede ser escrita de dos formas. La larga:
        // public String funcion(Tipo1 param1, Tipo2 param2){
        //  return unString;
        // }
        // Una forma "inline" osea corta:
        // (param1, param2) -> unString;
        List<Estudiante> estudiantes = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Estudiante(
                        rs.getInt("id"),
                        rs.getString("nombre"),
                        rs.getString("correo"),
                        rs.getInt("edad")
                ), new Object[]{this.getId()});
        if (estudiantes != null && estudiantes.size() > 0) {
            this.setId(estudiantes.get(0).getId());
            this.setNombre(estudiantes.get(0).getNombre());
            this.setCorreo(estudiantes.get(0).getCorreo());
            this.setEdad(estudiantes.get(0).getEdad());

            return true;
        } else {
            return false;            
        }
    }
    
    // Actualizar 
    public void actualizar() throws ClassNotFoundException, SQLException{
        // CRUD -U
        String sql = "UPDATE estudiantes SET nombre = ?, correo = ?, edad = ? WHERE id = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setString(1, this.getNombre());
        ps.setString(2, this.getCorreo());
        ps.setInt(3, this.getEdad());
        ps.setInt(4, this.getId());
        ps.executeUpdate();
        ps.close();
    }
    
    @Override
    public String toString() {
        return "Estudiante{" + "id=" + id + ", nombre=" + nombre + ", correo=" + correo + ", edad=" + edad + '}';
    }
    
}
