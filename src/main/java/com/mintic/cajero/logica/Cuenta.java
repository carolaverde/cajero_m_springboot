/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mintic.cajero.logica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Service;
//import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Tefa
 */
@Component("cuenta") // en SpringBoot los modelos pasan a ser Componentes
public class Cuenta {
    
    @Autowired
    transient JdbcTemplate jdbcTemplate;
    
    
    // Atributos
    private int id;
    private int numero_cuenta;
    private int cvv;
    private int clave;
    private float saldo;
    private int idEstudiante;
    
    // Constructor
    public Cuenta(int id, int numero_cuenta, int cvv, int clave, float saldo){
        super(); // Hereda de la clase principal
        this.id = id;
        this.numero_cuenta = numero_cuenta;
        this.cvv = cvv;
        this.clave = clave;
        this.saldo = saldo;
    }
    
    public Cuenta(){
        
    }

    // Métodos
    // obtener / consultar
    public int getId() {
        return id;
    }
    
    // set - modificar
    public void setId(int id) {
        this.id = id;
    }
    
    public int getNumero_cuenta() {
        return numero_cuenta;
    }
    
    public void setNumero_cuenta(int numero_cuenta) {
        this.numero_cuenta = numero_cuenta;
    }
    
    public int getCvv() {
        return cvv;
    }
    
    public void setCvv(int cvv) {
        this.cvv = cvv;
    }
    
    public int getClave() {
        return clave;
    }
    
    public void setClave(int clave) {
        this.clave = clave;
    }
    
    public float getSaldo() {
        return saldo;
    }
    
    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }
    
    public int getIdEstudiante(){
        return this.idEstudiante;
    }
    
    public void setIdEstudiante(int id_estudiante){
        this.idEstudiante = id_estudiante;
    }
    
    // CRUD
    // Guardar
    public String guardar() throws ClassNotFoundException, SQLException{
        String  sql = "INSERT INTO cuenta(id, numero_cuenta, cvv, clave) VALUES(?,?,?,?)";
        return sql;
    }
    
    // Consultar
    public boolean consultar() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        String  sql = "SELECT id,numero_cuenta,cvv,clave,saldo,id_estudiante FROM cuentas WHERE id_estudiante = ?";
        List<Cuenta> cuentas = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Cuenta(
                        rs.getInt("id"), // => 123
                        rs.getInt("numero_cuenta"), // => 
                        rs.getInt("cvv"),
                        rs.getInt("clave"),
                        rs.getFloat("saldo")
                ), new Object[]{this.getIdEstudiante()});
        if (cuentas != null &&  cuentas.size() > 0 ) {
            this.setId(cuentas.get(0).getId());
            this.setCvv(cuentas.get(0).getCvv());
            this.setNumero_cuenta(cuentas.get(0).getNumero_cuenta());
            this.setSaldo(cuentas.get(0).getSaldo());
            this.setClave(cuentas.get(0).getClave());
            
            return true;
        }
        else {
            return false;
        }
    }
    
    public List<Cuenta> consultarTodo(int id_estudiante) throws ClassNotFoundException, SQLException{
        //CRUD -R 
        String  sql = "SELECT id,numero_cuenta,cvv,clave,saldo,id_estudiante FROM cuentas WHERE id_estudiante = ?";
        List<Cuenta> cuentas = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Cuenta(//devuelve una instancia de la cuenta
                        rs.getInt("id"), // => 123
                        rs.getInt("numero_cuenta"), // => 
                        rs.getInt("cvv"),
                        rs.getInt("clave"),
                        rs.getFloat("saldo")
                ), new Object[]{ id_estudiante });// id,cvv.....
        
        return cuentas;
    }
    
    // Actualizar 
    public String actualizar() throws ClassNotFoundException, SQLException{
        // CRUD -U
        String sql = "UPDATE cuentas SET numero_cuenta = ?,cvv = ?, clave = ?,saldo = ? WHERE id = ?";
        return sql;
    }
    
    // Borrar
    public boolean borrar() throws SQLException, ClassNotFoundException{
        //CRUD -D
        String sql = "DELETE FROM cuentas WHERE id = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.getId());
        ps.execute();
        ps.close();
        
        return true;
   
    }

    // Sobre escribir el metodo ToString para ver los valor de los atributos de una clase
    @Override
    public String toString() {
        return "Cuenta{" + "id=" + id + ", numero_cuenta=" + numero_cuenta + ", cvv=" + cvv + ", clave=" + clave + ", saldo="+ saldo + '}';
    }    
}
