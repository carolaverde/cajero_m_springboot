package com.mintic.cajero;

import com.mintic.cajero.logica.Cuenta;
import com.mintic.cajero.logica.Estudiante;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/*import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;*/

@SpringBootApplication // Donde se encuentre este arroba, es el Main.
@RestController // Indica que la clase será un API REST.
public class CajeroApplication {

    @Autowired // Sirve para interpretar una clase sin necesidad de decirle que cree una instancia nueva. Equivalente a c = new Cuenta();
    Cuenta c; // Instancias
    @Autowired
    Estudiante e; // Instancia
    // Ventajas: manejo de excepciones controlado.
    // Evita el SQL Injection que pueden ingresar los hackers porque valida las entradas de los parametros.

    public static void main(String[] args) {
        SpringApplication.run(CajeroApplication.class, args);
    }

// EJEMPLO
    @GetMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name,
            @RequestParam(value = "edad", defaultValue = "20") String edad) {
        return String.format("Hello %s, you are %s old!", name, edad);
    }
    

    @GetMapping("/estudiante")
    public String consultarEstudiantePorCedula(@RequestParam(value = "cedula", defaultValue = "1") int cedula) throws ClassNotFoundException, SQLException {
        e.setId(cedula);
        if (e.consultar()) {
            String res = new Gson().toJson(e);
            e.setId(0);
            e.setNombre("");
            e.setEdad(0);
            e.setCorreo("");
            return res;
        } else {
            
            return new Gson().toJson(e);
        }
    }

    @GetMapping("/cuenta")
    public String consultarUnaCuentaPorEstudiante(@RequestParam(value = "cedula", defaultValue = "1") int cedula) throws ClassNotFoundException, SQLException {
        c.setIdEstudiante(cedula);
        if (c.consultar()) {
            return new Gson().toJson(c);
        } else {
            return new Gson().toJson(c);
        }
    }
    
    @GetMapping("/cuentas")
    public String consultarCuentasPorEstudiante(@RequestParam(value = "cedula", defaultValue = "1") int cedula) throws ClassNotFoundException, SQLException {
        
        List<Cuenta> cuentas = c.consultarTodo(cedula);
        if(cuentas.size() > 0){
            return new Gson().toJson(cuentas);
            //return "{\"id\":\""+c.getId()+"\",\"numcuenta\":\""+c.getNumero_cuenta()+"\",\"saldo\":\""+c.getSaldo()+"\",\"cvv\":\""+c.getCvv()+"\"}";
        } else {
            return new Gson().toJson(cuentas);
            //return "{\"id\":\""+cedula+"\",\"numcuenta\":\""+"La cédula no tiene asociada una cuenta."+"\"}";
        }   
    }
    
    // GET - POST - PUT - DELETE
    @PostMapping(path = "/estudiante", // le enviamos datos al servidor
        consumes = MediaType.APPLICATION_JSON_VALUE, // recibe json Nombre= Carlos
        produces = MediaType.APPLICATION_JSON_VALUE) // genera un json  => {nombre = Carlos}
    // definición
    
    // {nombre = Andres, edad = 15}
    public String actualizarEstudiante(@RequestBody String estudiante) throws ClassNotFoundException, SQLException{
                
        Estudiante f = new Gson().fromJson(estudiante, Estudiante.class); // recibimos el json y lo devolvemos un objeto estudiante
        e.setId(f.getId()); // obtengo el id de f y se lo ingreso e
        e.setCorreo(f.getCorreo());
        e.setEdad(f.getEdad());
        e.setNombre(f.getNombre());
        e.actualizar();
        return new Gson().toJson(e); // cliente le envia json al servidor. fpr de comunicarse
        
    }
    @DeleteMapping("/eliminarcuenta/{id}")
    public String borrarCuenta(@PathVariable("id") int id) throws SQLException, ClassNotFoundException{
        c.setId(id);
        c.borrar();
        return "Los datos del id indicado han sido eliminados";
    }
}
